FROM python:slim

# Python environment variables
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONUNBUFFERED="1"

# Temporary? Need to build dependencies for now
RUN apt update -y
RUN apt upgrade -y
RUN apt install -y gcc

# Copy ozcord into /app
WORKDIR /app
COPY ozcord /app

# Install bot dependencies
RUN pip install -r ./requirements.txt

# Temporary? Remove build dependencies
RUN apt remove --purge -y gcc
RUN apt autoremove -y

# Set up environment variables for the bot
ENV TOKEN=""
ENV PREFIX="ozb!"

CMD python3 -m ozcord --token $TOKEN --prefix $PREFIX
